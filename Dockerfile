FROM debian:testing-slim

RUN apt-get update && \
    apt-get install -y afl g++ cmake git 

RUN git clone https://github.com/catchorg/Catch2.git
WORKDIR Catch2
RUN cmake -Bbuild -H. -DBUILD_TESTING=OFF
RUN cmake --build build/ --target install

COPY ./ /root/
WORKDIR /root

CMD make && make bin/tests && bin/tests && make bin/fuzz_main && make fuzz
