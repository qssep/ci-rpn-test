with import (import ./nixpkgs.nix) {};

let
  fuzz_derivation = import ./make_fuzzbin.nix;
in
  derivation {
    system     = builtins.currentSystem;
    name       = "makeStdBin";
    builder    = "${pkgs.bash}/bin/bash";
    args       = [ ./make_fuzz.sh ];
    source     = ./.;
    fuzz_bin   = fuzz_derivation.out;
    gccPath    = "${pkgs.gcc}/bin:${pkgs.afl}/bin/";
    mkdir_path = "${pkgs.coreutils}/bin/";
    make       = "${pkgs.gnumake}/bin/make";
    cp         = "${pkgs.coreutils}/bin/cp";
    rm         = "${pkgs.coreutils}/bin/rm";
    kill       = "${pkgs.coreutils}/bin/kill";
  }
