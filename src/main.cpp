#include "main.h"
#include <climits>
#include <iostream>
#include <iterator>
#include <stack>
#include <stdexcept>
#include <unordered_map>

/*
 * Adds two integers
 */
int add(const int a, const int b) {
  int res;
  bool overflow = __builtin_sadd_overflow(a, b, &res);
  if (overflow) {
    throw std::invalid_argument("Malformed input. Integer overflow (add).");
  }
  return res;
}

/*
 * Multiplies two integers
 */
int multiply(const int a, const int b) {
  int res;
  bool overflow = __builtin_smul_overflow(a, b, &res);
  if (overflow) {
    throw std::invalid_argument("Malformed input. Multiplication error.");
  }
  return res;
}

/*
 * Subtracts integer b from integer a
 */
int subtract(const int a, const int b) {
  int res;
  bool overflow = __builtin_ssub_overflow(b, a, &res);
  if (overflow) {
    throw std::invalid_argument("Malformed input. Integer overflow (subtract).");
  }
  return res;
}

/*
 * Divides integer b from integer a, throwing an exception when b is zero.
 * Throws an std::invalid_argument exceptions if invalid parameters are provided.
 */
int divide(const int a, const int b) {
  if (b == 0) {
    throw std::invalid_argument("Malformed input. Division by zero.");
  }
  return a / b;
}

using binary_operator = decltype(&add);

int evaluate(const std::string &s) {
  static const std::unordered_map<char, binary_operator> map {
    {'+', add},
    {'-', subtract},
    {'*', multiply},
    {'/', divide}
  };
  std::stack<int> stack;

  for (char c : s) {
    if (std::isdigit(c)) {
      stack.push(c - '0');
    } else if (stack.size() >= 2 ) {
        const auto iter = map.find(c);
        if (iter != map.end()) {
          const auto a = stack.top();
          stack.pop();
          const auto b = stack.top();
          stack.pop();
          stack.push(iter->second(b, a));
        } else {
          throw std::invalid_argument("Invalid character found.");
        }
    } else {
      throw std::invalid_argument("Invalid number of digits.");
    }
  }
  if (stack.size() > 1 || stack.size() == 0) {
    throw std::invalid_argument("Invalid input.");
  }
  return stack.top();
}
