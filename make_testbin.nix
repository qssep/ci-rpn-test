with import (import ./nixpkgs.nix) {};

derivation {
  system     = builtins.currentSystem;
  name       = "makeStdBin";
  builder    = "${pkgs.bash}/bin/bash";
  args       = [ ./make_tests.sh ];
  source     = ./.;
  gccPath    = "${pkgs.gcc}/bin/";
  CXXFLAGS   = " -I${pkgs.catch2}/include -fsanitize=undefined ";
  mkdir_path = "${pkgs.coreutils}/bin/";
  make       = "${pkgs.gnumake}/bin/make";
  cp         = "${pkgs.coreutils}/bin/cp";
}
