with import <nixpkgs> {};

let
stdbin = derivation {
  system     = builtins.currentSystem;
  name       = "makeStdBin";
  builder    = "${pkgs.bash}/bin/bash";
  args       = [ ./make_stdbin.sh ];
  source     = ./.;
  gccPath    = "${pkgs.gcc}/bin/";
  mkdir_path = "${pkgs.coreutils}/bin/";
  make       = "${pkgs.gnumake}/bin/make";
  cp         = "${pkgs.coreutils}/bin/cp";
};

testbin = derivation {
  system     = builtins.currentSystem;
  name       = "makeStdBin";
  builder    = "${pkgs.bash}/bin/bash";
  args       = [ ./make_tests.sh ];
  source     = ./.;
  gccPath    = "${pkgs.gcc}/bin/";
  CXXFLAGS   = " -I${pkgs.catch2}/include -fsanitize=undefined ";
  mkdir_path = "${pkgs.coreutils}/bin/";
  make       = "${pkgs.gnumake}/bin/make";
  cp         = "${pkgs.coreutils}/bin/cp";
};

fuzz_derivation = derivation {
  system     = builtins.currentSystem;
  name       = "makeStdBin";
  builder    = "${pkgs.bash}/bin/bash";
  args       = [ ./make_fuzzbin.sh ];
  source     = ./.;
  gccPath    = "${pkgs.gcc}/bin:${pkgs.afl}/bin/";
  mkdir_path = "${pkgs.coreutils}/bin/";
  make       = "${pkgs.gnumake}/bin/make";
  cp         = "${pkgs.coreutils}/bin/cp";
};

in
  derivation {
    system     = builtins.currentSystem;
    name       = "makeStdBin";
    builder    = "${pkgs.bash}/bin/bash";
    args       = [ ./make_fuzz.sh ];
    source     = ./.;
    fuzz_bin   = fuzz_derivation.out;
    gccPath    = "${pkgs.gcc}/bin:${pkgs.afl}/bin/";
    mkdir_path = "${pkgs.coreutils}/bin/";
    make       = "${pkgs.gnumake}/bin/make";
    cp         = "${pkgs.coreutils}/bin/cp";
    rm         = "${pkgs.coreutils}/bin/rm";
    kill       = "${pkgs.coreutils}/bin/kill";
  }
