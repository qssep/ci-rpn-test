with import (import ./nixpkgs.nix) {};

derivation {
  system     = builtins.currentSystem;
  name       = "makeStdBin";
  builder    = "${pkgs.bash}/bin/bash";
  args       = [ ./make_fuzzbin.sh ];
  source     = ./.;
  gccPath    = "${pkgs.gcc}/bin:${pkgs.afl}/bin/";
  mkdir_path = "${pkgs.coreutils}/bin/";
  make       = "${pkgs.gnumake}/bin/make";
  cp         = "${pkgs.coreutils}/bin/cp";
}
