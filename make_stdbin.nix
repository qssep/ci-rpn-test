with import <nixpkgs> {};

derivation {
  system     = builtins.currentSystem;
  name       = "makeStdBin";
  builder    = "${pkgs.bash}/bin/bash";
  args       = [ ./make_stdbin.sh ];
  source     = ./.;
  gccPath    = "${pkgs.gcc}/bin/";
  mkdir_path = "${pkgs.coreutils}/bin/";
  make       = "${pkgs.gnumake}/bin/make";
  cp         = "${pkgs.coreutils}/bin/cp";
}
