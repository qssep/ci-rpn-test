PATH=$gccPath:$mkdir_path
$mkdir_path/mkdir -p $out
$mkdir_path/mkdir -p $out/bin/
$mkdir_path/mkdir -p ./bin/

$mkdir_path/mkdir -p ./test/out

$cp -R $source/* ./
$cp -R $fuzz_bin/bin/* ./bin/

$rm -rf test/out/
afl-fuzz -i test/in -o $out -- ./bin/fuzz_main &
PID=$!
sleep 10
$kill $PID

